import Home from '../pages/home.vue'
import Login from '../components/auth/Login.vue'
import Default from '../pages/default'
import Registro from '../components/auth/Registro.vue'
import Recover from '../components/auth/Recover.vue'

export default [
    {
        path: '/',

        component: Default,
        children: [
            {
                path: '',
                name: 'login',
                component: Login,
                meta: { requireAuth: false }
            },
            {
                path: '/signin',
                name: 'Signin',
                component: Registro,
                meta: { requireAuth: false }
            },
            {
                path: '/app',
                name: 'App',
                component: Home,
                meta: { requireAuth: false }
            },
            {
                path: '/recover',
                name: 'Recover',
                component: Recover,
                meta: { requireAuth: false }
            }
        ]
    },

]