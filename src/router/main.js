
import Vue from 'vue'
import Router from 'vue-router'
import Parse from 'parse'
import routes from './routes'





Vue.use(Router)
const router = new Router({
    routes
})

router.beforeEach((to, from, next) => {
    const user = Parse.User.current()
    // if (to.path.includes('/app') && !user) {
    //     next('/')
    // } else {
    //     next()
    // }
    if (to.meta.requireAuth && !user) {
        next('/')
    } else if (!to.meta.requireAuth && user) {
        next('/app')
    } else {
        next()
    }

    console.log(Parse.User.current())
    //next()
})
export default router