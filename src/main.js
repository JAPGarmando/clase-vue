import Vue from 'vue'
import App from './App.vue'
import store from './store/main.js'
import router from './router/main.js'
import vuetify from './plugins/vuetify';
import Parse from 'parse'


const app = process.env.VUE_APP_PARSE_SERVER_APPLICATION_ID
const js = process.env.VUE_APP_PARSE_SERVER_JS_KEY

console.log(app)
Parse.initialize(app, js)
Parse.serverURL = process.env.VUE_APP_PARSE_SERVER_URL



Vue.prototype.$Parse = Parse
Vue.prototype.$pUser = Parse.User.current()
Vue.config.productionTip = false



Vue.config.productionTip = false

new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
