import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        alert: {
            show: false,
            type: 'success',
            msg: ''
        },
        snackbar: {
            show: false,
            type: 'success',
            msg: ''
        }

    },
    mutations: {
        setAlert(state, data) {
            if (data) {
                state.alert = {
                    show: true,
                    type: data.type,
                    msg: data.msg
                }
            } else {
                state.alert = {
                    show: false,
                    type: 'success',
                    msg: ''
                }
            }
        },
        setSnackbar(state, data) {
            if (data) {
                state.snackbar = {
                    show: true,
                    type: data.type,
                    msg: data.msg
                }
            } else {
                state.snackbar = {
                    show: false,
                    type: 'success',
                    msg: ''
                }
            }
        }

    },
    actions: {
        showAlert({ commit }, data) {
            commit('setAlert', data)
            setTimeout(() => {
                commit('setAlert')
            }, 3500)

        },
        showSnackbar({ commit }, data) {
            commit('setSnackbar', data)
            setTimeout(() => {
                commit('setSnackbar')
            }, 3500)

        }
    },
})